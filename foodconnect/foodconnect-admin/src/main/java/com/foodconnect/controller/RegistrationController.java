package com.foodconnect.controller;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.foodconnect.model.Registration;
import com.foodconnect.service.RegistrationService;
import com.foodconnect.util.IConstant;

@Controller
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    /**
     * Save customer information
     * @param registration
     * @param response
     * @return
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public @ResponseBody Map<Object, Object> save(@RequestBody Registration registration, HttpServletResponse response) {
        Map<Object, Object> registrationMap = new HashMap<Object, Object>();
        boolean status = registrationService.save(registration);
        if (status) {
            registrationMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
            registrationMap.put(IConstant.MESSAGE, IConstant.REGISTRATION_SUCCESSFUL);
        } else {
            registrationMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
            registrationMap.put(IConstant.MESSAGE, IConstant.REGISTRATION_FAILURE);
        }
        return registrationMap;
    }

    /**
     * Find by email and password
     * @param registration
     * @param response
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public @ResponseBody Map<Object, Object> login(@RequestBody Registration registration, HttpServletResponse response) {
        Map<Object, Object> loginMap = new HashMap<Object, Object>();
        Registration result = registrationService.findByEmailAndPassword(registration.getEmail(),
                        registration.getPassword());
        if (result != null) {
            loginMap.put(IConstant.RESPONSE, IConstant.RESPONSE_SUCCESS_MESSAGE);
            loginMap.put(IConstant.DATA, result);
        } else {
            loginMap.put(IConstant.RESPONSE, IConstant.RESPONSE_NO_DATA_MESSAGE);
            loginMap.put(IConstant.MESSAGE, IConstant.LOGIN_FAILURE);
        }
        return loginMap;
    }
}
