package com.foodconnect.util;

public class IConstant {

    public static final String REGISTRATION_SUCCESSFUL = "Registration successful.";
    public static final String REGISTRATION_FAILURE = "Please try again";
    public static final String LOGIN_FAILURE = "Invalid login and password";
    public static final int IS_APPROVED = 1;
    public static final int IS_NOT_APPROVED = 0;
   
    /* It is used for login web service for web service */
    public static final String RESPONSE = "response";
    public static final String DATA = "DATA";
    public static final String MESSAGE = "MESSAGE";
    public static final String RESPONSE_SUCCESS_MESSAGE = "200";
    public static final String RESPONSE_NO_DATA_MESSAGE = "400";

   
}
