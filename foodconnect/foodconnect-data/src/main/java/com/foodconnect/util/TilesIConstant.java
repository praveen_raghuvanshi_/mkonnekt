package com.foodconnect.util;

public class TilesIConstant {

    /**
     * User header and footer
     */
    public static final String DEFAULT_LAYOUT = "/WEB-INF/layout/defaultLayout.jsp";
    public static final String TITLE = "FoodConnect";
    public static final String HEADER = "/WEB-INF/layout/header.jsp";
    public static final String FOOTER = "/WEB-INF/layout/footer.jsp";

    public static final String WELCOME_PAGE = "/WEB-INF/pages/welcome.jsp";
}
