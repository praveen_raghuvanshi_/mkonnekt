package com.foodconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodconnect.model.Registration;

public interface RegistrationRepository extends JpaRepository<Registration, Integer> {

    /**
     * Find by email and password
     * 
     * @param email
     * @param password
     * @return Registration instance
     */
    Registration findByEmailAndPassword(String email, String password);

}
