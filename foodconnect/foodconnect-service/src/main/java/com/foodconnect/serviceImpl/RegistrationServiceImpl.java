package com.foodconnect.serviceImpl;

import java.util.List;

import com.foodconnect.model.Registration;
import com.foodconnect.repository.RegistrationRepository;
import com.foodconnect.service.RegistrationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private RegistrationRepository registrationRepository;

    public boolean save(Registration registration) {
        registrationRepository.save(registration);
        return true;
    }

    public List<Registration> getAll() {
        // TODO Auto-generated method stub
        return null;
    }

    public Registration update(int id) {
        // TODO Auto-generated method stub
        return null;
    }

    public void delete(int id) {
        // TODO Auto-generated method stub

    }

    public List<Registration> findById(int id) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Find by email and passowrd
     */
    public Registration findByEmailAndPassword(String email, String password) {
        return registrationRepository.findByEmailAndPassword(email, password);
    }

}
