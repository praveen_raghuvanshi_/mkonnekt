package com.foodconnect.service;

import com.foodconnect.model.Registration;

public interface RegistrationService extends Service<Registration> {

    /**
     * Find by email and password
     * 
     * @param email
     * @param password
     * @return Registration instance
     */
    Registration findByEmailAndPassword(String email, String password);

}
